# Fun With Theoretical Math: Fibonacci Sequence

## Summary
When I was first learning to code with Ruby, I was asked to iteratively and recursively calculate the nth number in a Fibonacci Sequence. Playing around with the Ruby documentation, I found a better way.

### The Fibonacci Sequence
The [Fibonacci sequence](http://en.wikipedia.org/wiki/Fibonacci_number) is a specific sequence of numbers.  The numbers in the sequence are observable in nature (if interested, see video [Parts 1](http://www.youtube.com/watch?v=ahXIMUkSXX0), [2](http://www.youtube.com/watch?v=lOIP_Z_-0Hs), and [3](http://www.youtube.com/watch?v=14-NdQwKz9w)), and the ratio between the numbers approximates the [Golden Ratio](https://en.wikipedia.org/wiki/Golden_ratio) which has been [used in the arts](https://en.wikipedia.org/wiki/List_of_works_designed_with_the_golden_ratio) for its aesthetic properties.

The sequence is built by following simple rules:

- The sequence starts with 0 and 1.
- The next number in the sequence is the sum of the previous two numbers in the sequence.

![building the Fibonacci sequence](readme-assets/build_fibonacci_sequence.gif)

*Building the Fibonacci sequence.*

## Benchmarking for some BIG Fibonacci Numbers
### Iteratively Calculating the 100th Fibonacci Number
I wrote a method that returns the nth number in the Fibonacci Sequence.  We're going to test it out with the 100th in the sequence, which is a gigantic number in the hundreds of quintillions range (354,224,848,179,261,915,075)

```ruby
def iterative_nth_fibonacci_number(n)
  sequence = [0,1]
  if n != 0 && n != 1
    until sequence.length > n
      sequence << sequence[-1] + sequence[-2]
    end
    sequence.last
  else
    n
  end
end
```

Benchmark results are as follows:
```ruby
puts Benchmark.measure { iterative_nth_fibonacci_number(100) }
  0.000000   0.000000   0.000000 (  0.000019)
```
...Pretty efficient at about 1/100th of a milisecond, and we receive the expected result!



### Recursively Calculating the 100th Fibonacci Number
Next, I've implemented the same behavior using a recursive algorithm to calculate and return the nth number in the Fibonacci Sequence, and again we are looking for the value of the 100th Fibonacci Number (354,224,848,179,261,915,075)

```ruby
def recursive_nth_fibonacci_number(n)
  raise ArgumentError.new("Cannot process negative integer") if n < 0
  return n if n == 0 || n == 1
  recursive_nth_fibonacci_number(n-1) + recursive_nth_fibonacci_number(n-2) if n > 1
end
```

Benchmark results are as follows:
```ruby
...
```

...well I gave it a full 10 minutes on my 2016 Macbook Pro, but it never got close. Why is that? Probably because I told my poor laptop to do 100! arithmetic calculations (*100!* is *93,326,215,443,944,152,681,699,238,856,266,700,490,715,968,264,381,621,468,592,963,895,217,599,993,229,915,608,941,463,976,156,518,286,253,697,920,827,223,758,251,185,210,916,864,000,000,000,000,000,000,000,000*)!

Let's give it a try with calculating the 30th number in a Fibonacci Sequence and see if it can at least handle that?
```ruby
puts Benchmark.measure { recursive_nth_fibonacci_number(30) }
  0.170000   0.000000   0.170000 (  0.171547)
```
...and even that took it a human perceptible 170 milliseconds, or about 1/5th of a second. There must be a better way!


### A Smarter Approach To Calculating Absurd Numbers
Finally, I leveraged Ruby's Hash construct to cache the results.

```ruby
FIBONACCI = Hash.new

def hashy_fib(n)
  return n if n <= 1
  return FIBONACCI[n] if FIBONACCI[n]
  FIBONACCI[n] = hashy_fib(n-1) + hashy_fib(n-2)
end
```


Benchmark results are as follows:
```ruby
puts Benchmark.measure { hashy_fib(100) }
  0.000000   0.000000   0.000000 (  0.000060)
```

...dramatically improved, no?

Let's see how long it takes to retrieve Fibonacci 7000:
```ruby
puts Benchmark.measure { hashy_fib(7000) }
  0.000000   0.000000   0.000000 (  0.008869)
```

...Yep, much better than that recursive approach!

That said, it's no better than the iterative approach to reaching Fibonacci 7000:
```ruby
puts Benchmark.measure { iterative_nth_fibonacci_number(7000) }
  0.000000   0.000000   0.000000 (  0.005978)
```

...except that once the hash-caching method has calculated the 7000th number in a Fibonacci Sequence, it can return it, and any value less than it in an extraordinarily short amount of time because it retains the calculations!
```ruby
puts Benchmark.measure { hashy_fib(7000) }
  0.000000   0.000000   0.000000 (  0.000005)
```

Additionally, calculating further values in a Fibonacci Sequence is dramatically improved as much of the work has already been done, and is stored:
```ruby
puts Benchmark.measure { hashy_fib(7001) }
  0.000000   0.000000   0.000000 (  0.000009)
```


Incidentally, the 7000th number in a Fibonacci Sequence is as follows:
*36643483050372328322763589672816049218571543934175989626270698720728011459961452615205304474088508634285133393772080143860609858437637289909505603382510796045818812761764843963097882756899306880632339149624457792521065549662450746982954629516070098764978344151183599533003076277908774345939181724390901980527597663311555613033194153844866587511336793498907902783405698117902719459066855627353047337434107530829780633602911908426389755252823713762551462513907377077479794770248229483843646633681833549215123470585482715472809087383941758904346522640847918233307726932886610834511442709077969599000511722444264347175538294548185363876202654698511562719377096542631053943527028083011034249574050544328985535168955291671366036244479158743237803279520401188053252788470288847800035137267512317663342926011439333402801452136904199295820198703432837127533865033077917441010089802284149246910370407338605473066356582210888458052852205569170152356850628426912688415908336529818984499627245623589210650557134791498344835054794775623211187554679169981434112608914574843324668332643775010924705690019102274575145613483922681596385654759233269444264241619526680808134173728034947587278323338845059843941403722357555875001230291335579485064878430855934357730321907669366710759730431158502094644082670389464425638942201877393180553647515377317433628216098889451332718516720207276058883659104584529812229076091130643430114436305262385420314059450210042195431947096493318081320875*

## Conclusion
This challenge presented me with an opportunity to play with iterative and recursive ways of solving a real-world theoretical math problem, and a little exploration yielded smarter ways to get the same result, but dramatically more efficiently if recalling previously calculated values!
