def iterative_nth_fibonacci_number(n)
  sequence = [0,1]
  if n != 0 && n != 1
    until sequence.length > n
      sequence << sequence[-1] + sequence[-2]
    end
    sequence.last
  else
    n
  end
end

def recursive_nth_fibonacci_number(n)
  raise ArgumentError.new("Cannot process negative integer") if n < 0
  return n if n == 0 || n == 1
  recursive_nth_fibonacci_number(n-1) + recursive_nth_fibonacci_number(n-2) if n > 1
end

FIBONACCI = Hash.new

def hashy_fib(n)
  return n if n <= 1
  return FIBONACCI[n] if FIBONACCI[n]
  FIBONACCI[n] = hashy_fib(n-1) + hashy_fib(n-2)
end

# p iterative_nth_fibonacci_number(2)
# p recursive_nth_fibonacci_number(3)
# p hashy_fib(10)
